#include "scheduler.h"



int main(int argc, char const *argv[]){

	
	Bitacora *t1 = crear_Task( "Tarea_1", "04 marzo 2020","Reproducir M�sica");
	Bitacora *t2 = crear_Task("Tarea_2",  "05 marzo 2020","Abrir Youtube");
	Bitacora *t3 = crear_Task( "Tarea_3",  "06 marzo 2020","Escribir Texto en Word");
	Bitacora *t4 = crear_Task( "Tarea_4",  "07 marzo 2020","Descargando Archivo");

	Bitacora *array = (Bitacora*)malloc(sizeof(Bitacora)*MAX_TASK);

	agregar_Task(array, *t1, 0);
	agregar_Task(array, *t2, 1);
	agregar_Task(array, *t3, 2);
	agregar_Task(array, *t4, 3);

	ejecutar_Tasks(array);

	free(t1);
	free(t2);
	free(t3);
	free(t4);

	return 0;
}
